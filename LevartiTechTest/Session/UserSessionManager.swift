//
//  SessionManager.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation

struct UserSessionManager {
    
    static let shared = UserSessionManager()
    
    private init() {
        
    }
    
    private enum Keys {
        static let loggedInUser = "kLoggedInUser"
    }
    
    var isLoggedIn: Bool {
        return loggedInUser != nil
    }
    
    var storage: UserDefaults {
        return .standard
    }
    
    var loggedInUser: User? {
        if let encodedUser = storage.object(forKey: Keys.loggedInUser) as? Data {
            let decoder = JSONDecoder()
            if let user = try? decoder.decode(User.self, from: encodedUser) {
                return user
            }
        }
        return nil
    }
    
    func saveUserToSession(user: User) {
        let encoder = JSONEncoder()
        if let encodedUser = try? encoder.encode(user) {
            storage.set(encodedUser, forKey: Keys.loggedInUser)
        }
    }
    
    func logOut() {
        storage.removeObject(forKey: Keys.loggedInUser)
    }
}
