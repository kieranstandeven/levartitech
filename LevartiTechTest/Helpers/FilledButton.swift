//
//  FilledButton.swift
//  LevartiTechTest
//
//  Created by Kieran Standeven on 25/07/21.
//

import Foundation
import UIKit

class FilledButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 4
    @IBInspectable var fillColor: UIColor = UIColor.blue
    @IBInspectable var highlighterColor: UIColor = UIColor.white

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    override func awakeFromNib() {
        customInit()
    }
    
    func customInit() {
        layer.borderWidth = 1
        layer.borderColor = fillColor.cgColor
        layer.cornerRadius = cornerRadius
        setTitleColor(highlighterColor, for: .normal)
        backgroundColor = fillColor
    }
    
    override var isHighlighted: Bool {
        didSet {
            if (isHighlighted) {
                backgroundColor = highlighterColor
                setTitleColor(fillColor, for: .normal)
            }
            else {
                backgroundColor = fillColor
                setTitleColor(highlighterColor, for: .normal)
            }
        }
    }
}
