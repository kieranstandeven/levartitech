//
//  UIViewController+Loading.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 24/07/21.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showLoading() {
        let viewController = LoadingViewController()
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }
    
    func hideLoading() {
        dismiss(animated: false, completion: nil)
    }
}
