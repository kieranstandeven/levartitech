//
//  UIViewController+Alerts.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation
import UIKit

extension UIViewController {
    
    static private let defaultOk = "OK"
    static private let defaultCancel = "Cancel"
    
    func displayOKAlert(title: String?, message: String?, okButtonText: String = defaultOk, onOk: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: okButtonText, style: UIAlertAction.Style.default, handler: { (_) in
            onOk?()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
