//
//  AppError.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 24/07/21.
//

import Foundation

struct AppError : LocalizedError
{
    var errorDescription: String? { return description }
    var failureReason: String? { return description }
    var recoverySuggestion: String? { return "" }
    var helpAnchor: String? { return "" }
    
    private var description : String

    init(_ description: String) {
        self.description = description
    }
}
