//
//  ApiService.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation
import Alamofire

protocol ApiService {
    func processApiRequest(apiRequest: ApiRequest, completion: @escaping CompletionBlock)
}

struct JsonPlaceholderService: ApiService {
    
    func processApiRequest(apiRequest: ApiRequest, completion: @escaping CompletionBlock) {
        
        if case let .jsonPlaceholder(path, method) = apiRequest {
            
            guard var urlComponents = URLComponents(string: apiRequest.baseUrl.absoluteString) else {
                fatalError("Invalid url provided \(apiRequest.baseUrl.absoluteString)")
            }
            urlComponents.path = path
            
            guard let url = urlComponents.url else { fatalError("Invalid url provided: \(urlComponents.debugDescription)") }
            
            AF.request(url,
                       method: method,
                       encoding: JSONEncoding.default)
                .validate(statusCode: 200..<300)
                .validate(contentType: ["application/json"])
                .responseData { (responseData) in
                    switch responseData.result {
                    case .success:
                        completion(.success(responseData.data))
                    case .failure(let error):
                        completion(.failure(error))
                    }
                }
        }
    }
}
