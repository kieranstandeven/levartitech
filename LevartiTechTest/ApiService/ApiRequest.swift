//
//  ApiRequest.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation
import Alamofire

typealias CompletionBlock = (Result<Data?, Error>) -> Void

enum ApiRequest {
    
    case jsonPlaceholder(path: String, method: HTTPMethod)
    
    var baseUrl: URL {
        
        switch self {
        case .jsonPlaceholder:
            guard let baseUrl = URL(string: "https://jsonplaceholder.typicode.com") else { fatalError() }
            return baseUrl
        }
    }
}
