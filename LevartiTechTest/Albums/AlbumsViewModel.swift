//
//  AlbumsViewModel.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 24/07/21.
//

import Foundation

enum AlbumsViewModelState {
    case ready
    case loading(pullToRefresh: Bool)
    case loaded(models: [AlbumTableViewCellModel])
    case error(error: Error)
}

class AlbumsViewModel {
    
    private(set) var state = AlbumsViewModelState.ready {
        didSet {
            delegate?.viewModelStateChanged()
        }
    }
    
    let repository: AlbumRepositoryProtocol
    let user: User
    
    var delegate: ViewModelDelegate?
    private var deletedAlbumIds: [Int] = []
    private var filteredModels:  [AlbumTableViewCellModel] = []
    
    var models: [AlbumTableViewCellModel] {
        return filteredModels
    }
    
    init(with repository: AlbumRepositoryProtocol, user: User) {
        self.repository = repository
        self.user = user
        loadDeletedAlbumIds()
    }

    func loadAlbums(pullToRefresh: Bool) {
        state = .loading(pullToRefresh: pullToRefresh)
        
        repository.loadAlbums { [weak self] result in
            switch result {
            case .success(let albums):
                ///
                /// Filter out any delete models while mapping them
                ///
                let models: [AlbumTableViewCellModel] = albums.compactMap { [weak self] in
                    if !(self?.deletedAlbumIds.contains($0.id) ?? false) {
                        return AlbumTableViewCellModel(album: $0)
                    }
                    else {
                        return nil
                    }
                }
                
                self?.filteredModels = models
                self?.state = .loaded(models: models)
            case .failure(let error):
                self?.state = .error(error: error)
            }
        }
    }
    
    func reset() {
        state = .ready
    }

    func isAlbumDeleted(album: Album) -> Bool {
        return deletedAlbumIds.contains(album.id)
    }
    
    func delete(album: Album) {
        repository.addDeletedId(album: album)
        loadDeletedAlbumIds()
        
        // Update the filtered models now we have deleted one
        filteredModels = filteredModels.filter({ !deletedAlbumIds.contains($0.album.id) })
    }
    
    private func loadDeletedAlbumIds() {
        deletedAlbumIds = repository.readDeletedAlbumIds()
    }
}
