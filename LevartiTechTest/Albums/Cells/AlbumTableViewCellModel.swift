//
//  AlbumTableViewCellModel.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 24/07/21.
//

import Foundation
import AlamofireImage

struct AlbumTableViewCellModel {
    let album: Album
    
    func render(cell: AlbumTableViewCell) {
        
        cell.title.text = album.title
        if let url = album.thumbnailUrl {
            cell.albumImageView.image = nil
            cell.albumImageView.af.setImage(withURL: url, cacheKey: url.absoluteString)
        }
    }
}
