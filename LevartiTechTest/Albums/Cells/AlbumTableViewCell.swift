//
//  AlbumTableViewCell.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 24/07/21.
//

import Foundation
import UIKit

class AlbumTableViewCell: UITableViewCell {
    
    @IBOutlet var title: UILabel!
    @IBOutlet var albumImageView: UIImageView!    
}
