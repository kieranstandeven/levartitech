//
//  AlbumsViewController.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 24/07/21.
//

import Foundation
import UIKit

enum AlbumsZeroState {
    case hidden
    case loading
    case error(message: String?)
}

class AlbumsViewController: UIViewController, ViewModelDelegate {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var zeroStateView: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var errorMessage: UILabel!
    @IBOutlet var refreshButton: UIButton!

    let refreshControl = UIRefreshControl()

    let viewModel: AlbumsViewModel
    
    required init(viewModel: AlbumsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: AlbumsViewController.self), bundle: Bundle(for: type(of: self)))
    }
    
    override func viewDidLoad() {
        self.title = "Albums"
        navigationController?.setNavigationBarHidden(false, animated: false)
     
        let albumNib = UINib.init(nibName: "AlbumTableViewCell", bundle: Bundle(for: type(of: self)))
        tableView.register(albumNib, forCellReuseIdentifier: "AlbumTableViewCell")

        refreshControl.addTarget(self, action: #selector(self.pullToRefresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)

        updateZeroStateView(.loading)
        viewModel.delegate = self
        viewModel.loadAlbums(pullToRefresh: false)
    }
        
    func viewModelStateChanged() {
        switch viewModel.state {
        case .ready:
            updateZeroStateView(.loading)
        case .loading(let pullToRefresh):
            if !pullToRefresh {
                updateZeroStateView(.loading)
            }
        case .loaded(let models):
            if models.count > 0 {
                updateZeroStateView(.hidden)
                tableView.reloadData()
            }
            else {
                updateZeroStateView(.error(message: "No albums found"))
            }
            refreshControl.endRefreshing()
        case .error(let error):
            updateZeroStateView(.error(message: error.localizedDescription))
            refreshControl.endRefreshing()
        }
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func refreshTapped() {
        viewModel.loadAlbums(pullToRefresh: false)
    }
    
    @objc func pullToRefresh(_ sender: AnyObject) {
        viewModel.loadAlbums(pullToRefresh: true)
    }
}

/*
 Table view datasource
 */
extension AlbumsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableViewCell") as? AlbumTableViewCell else {
            return UITableViewCell()
        }
        
        let model = viewModel.models[indexPath.row]
        model.render(cell: cell)
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let model = viewModel.models[indexPath.row]
            viewModel.delete(album: model.album)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}

extension AlbumsViewController {
    func updateZeroStateView(_ state: AlbumsZeroState) {
        switch state {
        case .hidden:
            zeroStateView.isHidden = true
        case .loading:
            zeroStateView.isHidden = false
            activityIndicator.isHidden = false
            errorMessage.isHidden = true
            refreshButton.isHidden = true
        case .error(let message):
            zeroStateView.isHidden = false
            errorMessage.isHidden = false
            refreshButton.isHidden = false
            errorMessage.text = message
            activityIndicator.isHidden = true
        }
    }
}
