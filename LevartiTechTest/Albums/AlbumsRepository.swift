//
//  AlbumRepository.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation
import Alamofire

protocol AlbumRepositoryProtocol {
    
    typealias AlbumsCompletionBlock = (Result<[Album], Error>) -> Void

    func loadAlbums(completion: @escaping AlbumsCompletionBlock)
    func readDeletedAlbumIds() -> [Int]
    func addDeletedId(album: Album)
}

struct AlbumRepository: AlbumRepositoryProtocol {
    
    private enum Keys {
        static let deletedAlbumIds = "deletedAlbumIds"
    }
    
    func loadAlbums(completion: @escaping AlbumsCompletionBlock) {
        let apiRequest = ApiRequest.jsonPlaceholder(path: "/photos", method: HTTPMethod.get)
        JsonPlaceholderService().processApiRequest(apiRequest: apiRequest) { result in
            switch result {
            case .success(let data):
                if let responseData = data,
                    let albums = try? JSONDecoder().decode([Album].self, from: responseData) {
                    completion(.success(albums))
                }
                else {
                    completion(.success([]))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func readDeletedAlbumIds() -> [Int] {
        let userDefaults = UserDefaults.standard
        let stringIds: [String] = userDefaults.stringArray(forKey: Keys.deletedAlbumIds) ?? []
        return stringIds.compactMap { Int($0) }

    }
    
    func addDeletedId(album: Album) {
        let userDefaults = UserDefaults.standard
        var stringIds: [String] = userDefaults.stringArray(forKey: Keys.deletedAlbumIds) ?? []
        stringIds.append("\(album.id)")
        userDefaults.set(stringIds, forKey: Keys.deletedAlbumIds)
        userDefaults.synchronize()
    }
}
