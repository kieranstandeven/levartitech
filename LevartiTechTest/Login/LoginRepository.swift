//
//  LoginRepository.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation

protocol LoginRepositoryProtocol {
    typealias LoginCompletionBlock = (Result<User, Error>) -> Void
    
    func login(username: String, password: String, completion: @escaping LoginCompletionBlock)
}

struct LoginRepository: LoginRepositoryProtocol {
    
    func login(username: String, password: String, completion: @escaping LoginCompletionBlock) {
        
        ///
        /// Handle a real authentication endpoint here using the api service - if you used one
        ///
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            
            if username == "username" && password == "password" {
                let user = User(id: 1, username: username)
                completion(.success(user))
            }
            else {
                completion(.failure(AppError("Username or password is incorrect.")))
            }
        })
    }
}
