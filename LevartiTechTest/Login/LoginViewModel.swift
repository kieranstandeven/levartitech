//
//  LoginViewModel.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation

enum LoginViewModelState {
    case ready
    case loggingIn
    case error(error: Error)
    case loggedIn(user: User)
}

class LoginViewModel {
    
    private(set) var state = LoginViewModelState.ready {
        didSet {
            delegate.viewModelStateChanged()
        }
    }
    
    let repository: LoginRepositoryProtocol
    let delegate: ViewModelDelegate
    
    init(with repository: LoginRepositoryProtocol, delegate: ViewModelDelegate) {
        self.repository = repository
        self.delegate = delegate
    }

    func login(username: String, password: String) {
        state = .loggingIn

        repository.login(username: username, password: password) { [weak self] result in
            switch result {
            case .success(let user):
                /// So you don't have to login each time
                UserSessionManager.shared.saveUserToSession(user: user)
                
                self?.state = .loggedIn(user: user)
            case .failure(let error):
                self?.state = .error(error: error)
            }
        }
    }
    
    func reset() {
        state = .ready
    }
}
