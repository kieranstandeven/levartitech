//
//  LoginViewController.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation
import UIKit

class LoginViewController: UIViewController, ViewModelDelegate {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    /// Login view model requires no previous context, so we can just initialise it here
    lazy var viewModel: LoginViewModel = {
        return LoginViewModel(with: LoginRepository(), delegate: self)
    }()
            
    required init() {
        super.init(nibName: String(describing: LoginViewController.self), bundle: Bundle(for: type(of: self)))
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func viewModelStateChanged() {
        switch viewModel.state {
        case .ready:
            hideLoading()
        case .loggingIn:
            showLoading()
        case .error(let error):
            hideLoading()
            displayOKAlert(title: "Login Failed", message: error.localizedDescription)
            viewModel.reset()
        case .loggedIn(_):
            hideLoading()
            pushAlbumsViewController()
        }
    }
    
    func pushAlbumsViewController() {
        if let user = UserSessionManager.shared.loggedInUser {
            let albumsViewModel = AlbumsViewModel(with: AlbumRepository(), user: user)
            let viewController = AlbumsViewController(viewModel: albumsViewModel)
            navigationController?.setViewControllers([viewController], animated: false)
        }
    }
    
    @IBAction func loginPressed(sender: UIButton) {
        if let username = usernameTextField.text, let password = passwordTextField.text,
           username.count > 0, password.count > 0 {
            
            if usernameTextField.isFirstResponder {
                usernameTextField.resignFirstResponder()
            }
            else if passwordTextField.isFirstResponder {
                passwordTextField.resignFirstResponder()
            }
            
            viewModel.login(username: username, password: password)
        }
        else {
            displayOKAlert(title: "Missing Login Details", message: "You must enter a username and password")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


