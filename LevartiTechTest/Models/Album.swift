//
//  Album.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation

struct Album : Codable {
    var id: Int
    var albumId: Int
    var title: String?
    var url: URL?
    var thumbnailUrl: URL?
}
