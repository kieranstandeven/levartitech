//
//  User.swift
//  LevartiCodeTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation

struct User: Codable {
    let id: Int
    let username: String
}
