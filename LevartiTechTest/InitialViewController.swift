//
//  InitialViewController.swift
//  LevartiTechTest
//
//  Created by Kieran Standeven on 25/07/21.
//

import UIKit

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        var viewController: UIViewController
        
        if UserSessionManager.shared.isLoggedIn, let user = UserSessionManager.shared.loggedInUser {
            let albumsViewModel = AlbumsViewModel(with: AlbumRepository(),user: user)
            viewController = AlbumsViewController(viewModel: albumsViewModel)
        }
        else {
            viewController = LoginViewController()
        }
        
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .overCurrentContext
        present(navigationController, animated: false, completion: nil)

    }
}

