//
//  ViewModelDelegate.swift
//  LevartiTest
//
//  Created by Kieran Standeven on 23/07/21.
//

import Foundation

protocol ViewModelDelegate {
    
    func viewModelStateChanged()
}
