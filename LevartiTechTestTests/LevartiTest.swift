//
//  LevartiTestTests.swift
//  LevartiTestTests
//
//  Created by Kieran Standeven on 23/07/21.
//

import XCTest
import Hippolyte

@testable import LevartiTechTest

class LevartiTest: XCTestCase {

    func dataFromJson(jsonFileName: String) -> Data? {
        guard let jsonPath: String = Bundle.init(for: LevartiTest.classForCoder()).path(forResource: jsonFileName, ofType: "json") else { return nil }
        return try? Data(contentsOf: URL(fileURLWithPath: jsonPath))
    }
    
    func jsonFromFile(jsonFileName: String) -> Any? {
        guard let data = dataFromJson(jsonFileName: jsonFileName) else { return nil }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else { return nil }
        return json
    }
    
    func buildStubResponse(url: String, jsonFileName: String, httpMethod: HTTPMethod = .GET, responseCode: Int = 200) {
        guard let response = buildResponse(jsonFileName: jsonFileName, httpMethod: httpMethod, responseCode: responseCode) else {
            return
        }

        /// The request that will match this URL and return the stub response
        let requestBuilder = StubRequest.Builder()
            .stubRequest(withMethod: httpMethod, url: URL(string: url)!)
            .addResponse(response)
                
        /// Register the request
        let request = requestBuilder.build()
        Hippolyte.shared.add(stubbedRequest: request)
    }
    
    func buildResponse(jsonFileName: String, httpMethod: HTTPMethod = .GET, responseCode: Int = 200) -> StubResponse? {
        guard !jsonFileName.contains(".json") else {
            XCTFail("File name provided should not contain the `.json` file extension")
            return nil
        }
        let data = dataFromJson(jsonFileName: jsonFileName)!
        
        /// The stub response
        var response = StubResponse.Builder()
            .stubResponse(withStatusCode: responseCode)
            .addHeader(withKey: "Content-Type", value: "application/json")
            .build()
        response.body = data
        
        return response
    }

}
