//
//  AlbumsRepositoryTest.swift
//  LevartiTestTests
//
//  Created by Kieran Standeven on 25/07/21.
//

import XCTest
import Hippolyte

@testable import LevartiTechTest

class AlbumsRepositoryTest: LevartiTest {

    override func setUpWithError() throws {
        Hippolyte.shared.start()
        buildStubResponse(url: "https://jsonplaceholder.typicode.com/photos",
                      jsonFileName: "albums",
                      httpMethod: .GET)
    }

    override func tearDownWithError() throws {
        Hippolyte.shared.stop()
    }

    func testLoadingResponse() throws {
        
        let expectation = self.expectation(description: "Search network call")
        let repository: AlbumRepository = AlbumRepository()
        
        repository.loadAlbums() { result in
            
            switch result {
            case .success(let albums):
                XCTAssertEqual(albums.count, 5000)
                
                let sut = albums[2]
                
                XCTAssertEqual(sut.id, 3)
                XCTAssertEqual(sut.albumId, 1)
                XCTAssertEqual(sut.title, "officia porro iure quia iusto qui ipsa ut modi")
                XCTAssertEqual(sut.url?.absoluteString, "https://via.placeholder.com/600/24f355")
                XCTAssertEqual(sut.thumbnailUrl?.absoluteString, "https://via.placeholder.com/150/24f355")
                
                expectation.fulfill()
            case .failure(let error):
                XCTFail("failed with error: \(error.localizedDescription)")
            }
        }
        wait(for: [expectation], timeout: 1)
    }
}
