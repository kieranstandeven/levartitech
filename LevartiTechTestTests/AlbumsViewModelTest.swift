//
//  AlbumsViewModelTest.swift
//  LevartiTestTests
//
//  Created by Kieran Standeven on 25/07/21.
//

import XCTest

@testable import LevartiTechTest

class AlbumsViewModelTest: LevartiTest {

    var viewModel: AlbumsViewModel!
    
    override func setUpWithError() throws {
        let repository: MockAlbumRepository = MockAlbumRepository()
        let user = User(id: 0, username: "")
        viewModel = AlbumsViewModel(with: repository, user: user)
    }

    override func tearDownWithError() throws {
    }

    func testViewModelDataRetrieval() throws {
        var delegate = TestViewModelDelegate(viewModel: viewModel)
        delegate.expectation = self.expectation(description: "View model load call")
        viewModel.delegate = delegate
        
        viewModel.loadAlbums(pullToRefresh: false)
        
        wait(for: [delegate.expectation!], timeout: 1)
        
        XCTAssertEqual(viewModel.models.count, 5000)
        
        let sut = viewModel.models[2]
        XCTAssertEqual(sut.album.id, 3)
        XCTAssertEqual(sut.album.albumId, 1)
        XCTAssertEqual(sut.album.title, "officia porro iure quia iusto qui ipsa ut modi")
        XCTAssertEqual(sut.album.url?.absoluteString, "https://via.placeholder.com/600/24f355")
        XCTAssertEqual(sut.album.thumbnailUrl?.absoluteString, "https://via.placeholder.com/150/24f355")
    }
    
    func testViewModelDeletion() throws {
        var delegate = TestViewModelDelegate(viewModel: viewModel)
        delegate.expectation = self.expectation(description: "View model load call")
        viewModel.delegate = delegate
        viewModel.loadAlbums(pullToRefresh: false)
        wait(for: [delegate.expectation!], timeout: 1)
        
        XCTAssertEqual(viewModel.models.count, 5000)
        
        let sutAlbum = viewModel.models[2].album
        XCTAssertEqual(sutAlbum.id, 3)
        viewModel.delete(album: sutAlbum)
        
        XCTAssertEqual(viewModel.models.count, 4999)
        
        /// Verify album is now deleted
        let sutAlbum2 = viewModel.models[2].album
        XCTAssertEqual(sutAlbum2.id, 4)

    }
}

class MockAlbumRepository: LevartiTest, AlbumRepositoryProtocol {
    
    var deletedAlbumIds: [Int] = []
    
    func loadAlbums(completion: @escaping AlbumsCompletionBlock) {
        let data = dataFromJson(jsonFileName: "albums")!
        let albums = try! JSONDecoder().decode([Album].self, from: data)
        completion(.success(albums))
    }
    
    func readDeletedAlbumIds() -> [Int] {
        return deletedAlbumIds
    }
    
    func addDeletedId(album: Album) {
        deletedAlbumIds.append(album.id)
    }
}

struct TestViewModelDelegate: ViewModelDelegate {
    
    var expectation: XCTestExpectation? = nil
    let viewModel: AlbumsViewModel
    
    func viewModelStateChanged() {
        switch viewModel.state {
        case .loaded:
            expectation?.fulfill()
        default:
            return
        }
    }
}
